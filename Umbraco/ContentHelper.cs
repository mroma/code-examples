using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Umbraco.Core.Models;
using Umbraco.Web;
using Examine.LuceneEngine.SearchCriteria;

namespace www.mroma
{
    public class ContentHelper
    {        
        // function that returns a single document by id
        public static IPublishedContent GetContentById(int id)
        {
            // get the helper
            var helper = new UmbracoHelper(UmbracoContext.Current);
            return helper.TypedContent(id);
        }

        // function that queries for the given path and type
        public static IEnumerable<IPublishedContent> Query(string path, string type)
        {
            // define query
            Func<IEnumerable<IPublishedContent>> f = delegate()
            {
                // get the helper
                var helper = new UmbracoHelper(UmbracoContext.Current);

                // get examine
                var examine = Examine.ExamineManager.Instance;

                // build the criteria
                var criteria = examine.CreateSearchCriteria();

                // check if a type
                if (!String.IsNullOrEmpty(type))
                {
                    criteria.NodeTypeAlias(type);
                }

                // check if a path
                if (!String.IsNullOrEmpty(path))
                {
                    criteria.Field("__Path", path.MultipleCharacterWildcard());
                }

                // get the results
                var results = examine.Search(criteria);

                // return the list
                if (results != null)
                {
                    return helper.TypedContent(results.Select(x => x.Id)).Where(x => x != null).OrderBy(x => x.SortOrder);
                }

                // else, empty 
                return new List<IPublishedContent>();
            };

            // else, return
            return f();            
        }

        // function that query for the path of the given item and type
        public static IEnumerable<IPublishedContent> Query(IPublishedContent item, string type)
        {
            return Query(item.Path, type);
        }


        // function that finds the closest parent for the given type and given document
        public static IPublishedContent GetClosestParent(IPublishedContent item, string type)
        {
            // loop while a document
            while (item != null)
            {
                // check if the correct type
                if (item.DocumentTypeAlias == type)
                {
                    // return it
                    return item;
                }

                // go up
                item = item.Parent;
            }

            // if here, nothing
            return null;
        }

        // function that finds the closest parent for the given type and current document
        public static IPublishedContent GetClosestParent(string type)
        {
            // get the current document, if exists
            if (UmbracoContext.Current != null && UmbracoContext.Current.PageId.HasValue)
            {
                return GetClosestParent(GetContentById(UmbracoContext.Current.PageId.Value), type);
            }

            // else, null
            return null;
        }
    }
}