using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;

namespace www.mroma
{
    public class RelatedDocumentsManager
    {
        // define relation types
        public class RelationTypes
        {
            public const string RelateDocumentOnCopy = "relateDocumentOnCopy";
        }

        // events
        public class Events: ApplicationEventHandler
        {
            // construtor
            public Events()
            {
                // content publish
                ContentService.Published += ContentService_Published;

                // content copied
                ContentService.Copied += ContentService_Copied;
            }

            // content published
            void ContentService_Published(IPublishingStrategy sender, PublishEventArgs<IContent> e)
            {
                // get the services
                var rs = ApplicationContext.Current.Services.RelationService;
                var cs = ApplicationContext.Current.Services.ContentService;
                
                // go through each document published
                foreach (var c in e.PublishedEntities)
                {                    
                    // check if not the first level                
                    if (c.Level > 1 && !rs.GetByParentId(c.Id).Any())
                    {
                        // get the parent                    
                        foreach (var parentRelation in rs.GetByParentId(c.ParentId).Where(x => x.RelationType.Alias == RelationTypes.RelateDocumentOnCopy))
                        {
                            // copy the item if not already a relation
                            cs.Copy(c, parentRelation.ChildId, true);
                        }
                    }
                }
            }

            // content copied
            void ContentService_Copied(IContentService sender, CopyEventArgs<IContent> e)
            {
                // check if relating to original
                if (e.RelateToOriginal)
                {
                    // relate on copy
                    var rs = ApplicationContext.Current.Services.RelationService;
                    rs.Relate(e.Original, e.Copy, RelationTypes.RelateDocumentOnCopy);
                }
            }
        }
    }
}