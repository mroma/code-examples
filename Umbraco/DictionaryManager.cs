﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using www.mroma.Services;

namespace www.mroma
{
    public class DictionaryManager
    {
        // define prefix
        public const string Prefix = "mroma";

        // define dictionary items
        public enum Keys
        {            
            [DictionaryKey("PageTitle", "{0} | Site Name")] PageTitle
        }

        // function that returns a dictionary string
        public static string GetText(string key)
        {
            // get the service
            var locationService = ApplicationContext.Current.Services.LocalizationService;

            // find the dictionary item
            var item = locationService.GetDictionaryItemByKey(String.Concat(Prefix, key));
            if (item != null && item.Translations != null)
            {
                // get the current langiage
                int languageId = GetCurrentLanguage();

                // get the translation
                var t = item.Translations.FirstOrDefault(x => x.Language.Id == languageId);
                if (t == null)
                {
                    t = item.Translations.FirstOrDefault();
                }

                // check if a translation
                if (t != null)
                {
                    // return
                    return t.Value;
                }
            }

            // fallback to empty
            return "";
        }

        // function that gets the dictionary string
        public static string GetText(Keys key)
        {
            // get the dictionary key details
            var keyDetails = GetDetails((Keys)key);
            if( keyDetails != null)
            {
                return GetText(keyDetails.Name);
            }

            // fallback to empty
            return "";
        }

        // function that inits the dictionary and add new items if needed
        public static void Init()
        {
            // get the service
            var locationService = ApplicationContext.Current.Services.LocalizationService;

            // get all languages
            var langs = locationService.GetAllLanguages();

            // go through each key
            foreach(var key in Enum.GetValues(typeof(Keys)))
            {
                // get details, check if any
                var keyDetails = GetDetails((Keys)key);
                if (keyDetails != null)
                {
                    string keyName = String.Concat(Prefix, keyDetails.Name);
                    if (!locationService.DictionaryItemExists(keyName))
                    {
                        // init the new item
                        var newItem = new DictionaryItem(keyName);
                        newItem.Translations = langs
                            .Select(lang => new DictionaryTranslation(lang, keyDetails.DefaultText));                        

                        // save
                        locationService.Save(newItem);
                    }
                }
            }

        }

        // function that gets the current language
        public static int GetCurrentLanguage()
        {
            // get the curent page
            var currentPage = new UmbracoHelper(UmbracoContext.Current)
                .TypedContent(UmbracoContext.Current.PageId.Value);

            // check if a page
            if (currentPage != null)
            {
                // get the home page
                var home = currentPage.AncestorOrSelf(1);
                if (home != null)
                {
                    // get the domains for home
                    var domains = umbraco.cms.businesslogic.web.Domain.GetDomainsById(home.Id);
                    if (domains.Count() > 0)
                    {
                        return domains[0].Language.id;
                    }
                }
            }

            // if here, no language
            return 0;
        }



        // helper that gets the details
        private static DictionaryKeyAttribute GetDetails(Keys key)
        {
            return key.GetType()
                .GetField(key.ToString())
                .GetCustomAttributes(typeof(DictionaryKeyAttribute), false)
                .SingleOrDefault() as DictionaryKeyAttribute;            
        }
        
        // define the dictionary key attributes
        public class DictionaryKeyAttribute : Attribute
        {
            // define fields
            public string Name { get; set; }
            public string DefaultText { get; set; }

            // constructor
            public DictionaryKeyAttribute(string name, string defaultText)
            {
                Name = name;
                DefaultText = defaultText;
            }
        }        
    }
}